﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;

namespace IlmajaamaRakendus.Controllers
{
    public class DefaultController : ApiController
    {
        // GET: api/Default
        public string Get()
        {
            WebRequest wr = WebRequest.Create("https://weather.aw.ee/api/world/locations");
            wr.Method = "GET";
            HttpWebResponse resp = (HttpWebResponse)wr.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd();
            //return new string[] { "value1", "value2" };
        }

        // GET: api/Default/5
        public string Get(string id)
        {
            WebRequest wr = WebRequest.Create($"https://weather.aw.ee/api/world/locations/{id}");
            wr.Method = "GET";
            HttpWebResponse resp = (HttpWebResponse)wr.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd();
        }

        // POST: api/Default
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Default/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Default/5
        public void Delete(int id)
        {
        }
    }
}
